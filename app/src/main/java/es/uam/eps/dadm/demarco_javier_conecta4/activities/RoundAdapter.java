package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.Round;

/**
 * Created by javi on 31/03/18.
 */
public class RoundAdapter extends RecyclerView.Adapter<es.uam.eps.dadm.demarco_javier_conecta4.activities.RoundListFragment.RoundHolder> {
    private List<Round> rounds;

    public RoundAdapter(List<Round> rounds) {
        this.rounds = rounds;
    }

    @Override
    public es.uam.eps.dadm.demarco_javier_conecta4.activities.RoundListFragment.RoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_round, parent, false);
        return new RoundListFragment.RoundHolder(view);
    }

    @Override
    public void onBindViewHolder(es.uam.eps.dadm.demarco_javier_conecta4.activities.RoundListFragment.RoundHolder holder, int position) {
        Round round = rounds.get(position);
        holder.bindRound(round);
    }

    @Override
    public int getItemCount() {
        return rounds.size();
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }
}