package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.Round;
import es.uam.eps.multij.ExcepcionJuego;

public class RoundActivity extends AppCompatActivity implements RoundFragment.Callbacks {
    public static final String EXTRA_ROUND_ID = "es.uam.eps.dadm.demarco_javier_conecta4.round_id";
    public static final String BOARDSTRING = "es.uam.eps.dadm.demarco_javier_conecta4.grid";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            String roundId = getIntent().getStringExtra(EXTRA_ROUND_ID);
            RoundFragment roundFragment = RoundFragment.newInstance(roundId);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, roundFragment)
                    .commit();
        }
        PreferenceManager.setDefaultValues(this,R.xml.settings, false);
    }
    public static Intent newIntent(Context packageContext, String roundId){
        Intent intent = new Intent(packageContext, RoundActivity.class);
        intent.putExtra(EXTRA_ROUND_ID, roundId);
        return intent;
    }

<<<<<<< HEAD

    @Override
    public void onRoundUpdated() {
=======
    @Override
    public void onRoundUpdated(Round round) {
    return;
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
    }
}